---
title:      "Bulgur-Buckwheat Chili & Raw Salad [EN]"
lang:       en
categories: [salty, salad, whole-meal]
tags:       [whole-foods]
author:     robinv
#image:
#  path:      /assets/images/recipe_linzer-torte-3_cropped_long.jpg
#  thumbnail: /assets/images/recipe_linzer-torte-1.jpg
---

* korny chili
* big (fresh ;-) ) raw salad
* additional fresh raw stuff

# Ingredients

## Chili

* bulgur + buckwheat
* tomato puree + (red) kidney-beans + chili + herbs + beet root

## Salad

* beet root
* green cellery
* brocolli
* spring onions
* avocado
* pumpkin seeds
* carrots green

## Raw Snacks

* a boal of apples
* a boal of blue berries
* a boal with carrots and green cellery


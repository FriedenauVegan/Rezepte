﻿---
title:      "Raw Cake [EN]"
lang:       en
categories: [cakes, sweets, desserts]
tags:       [whole-foods, raw]
author:     robinv
image:
  path:      /assets/images/recipe_rohkost-kuchen-2_cropped_long.jpg
  thumbnail: /assets/images/recipe_rohkost-kuchen-3.jpg
---

# Crust (Boden)

## Ingredients

* water
* date-paste
* cacao-powder (just a bit to make it dark)
* oats (soaked over night)

## Steps

1. mix
2. spread out
3. put in fridge

# Filling

## Ingredients

* 1 cup of cashew nuts (soaked)
* beetroot (rote bete)
* berries or date-paste
* cinamon

or

* tofu
* avocado

or

* 500g carrots
* 1 cup of nuts (soaked)
* date-paste
* cinamon

or

* 3 frozen, very ripe bananas
* date-paste

or (NON RAW!)

* millet or amaranth cooked and cooled out
* ... something(s) to give it taste


# Topping

fruit of any kind!


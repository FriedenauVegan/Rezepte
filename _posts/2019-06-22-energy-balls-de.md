﻿---
title:      "Energy Balls"
categories: [sweets, desserts]
tags:       [whole-foods, raw]
author:     robinv
image:
  path:      /assets/images/recipe_energy-balls-1_cropped_long.jpg
  thumbnail: /assets/images/recipe_energy-balls-2.jpg
---

_Roh-Kost, Vegan, nix industrielles, ~~Gluten-Frei~~_

Kann gut eine Woche im Kühlschrank gelagert werden.

## Zutaten

* Rote-Bete
* Karotten
* Dattel-Paste
* Haferflocken
* Haselnüsse, fein gehackt
* Kakao-Pulver
* Zimt
* Sesam ODER Kokosnuss

## Anleitung

* Karotten & Rote-Bete fein reiben
* kneten um die Flüssigkeit auszudrücken
* Saft vom festen trennen (kann als Saft getrunken werde, aber Vorsicht: _sehr süss!_)
* Zimt beigeben und rein-kneten
* Dattel-Paste beigeben und rein-kneten
* Nüsse & Haferflocken beigeben und rein-kneten
* Kakao-Pulver beigeben und rein-kneten
* mit den Händen Kugeln formen und wahlweise in Sesam oder Kokosnuss wälzen

{% comment %}
evtl.:

* Nüsse im voraus einweichen
* + Ingwer
{% endcomment %}


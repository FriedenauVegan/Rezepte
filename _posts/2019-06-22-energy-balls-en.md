﻿---
title:      "Energy Balls [EN]"
lang:       en
categories: [sweets, desserts]
tags:       [whole-foods, raw]
author:     robinv
image:
  path:      /assets/images/recipe_energy-balls-1_cropped_long.jpg
  thumbnail: /assets/images/recipe_energy-balls-2.jpg
---

Can be kept in the fridge for a week.

## Ingredients

* beet-root
* carrots
* date-paste
* oatmeal
* hazelnuts, finely grated
* cacao powder
* cinnamon
* sesame OR coconut flakes

## How-To

* Finely grate carrots & beet root
* knead to press out the juice
* separate the juice (you may use it as a drink, but beware: _very sweet!_)
* add cinnamon and knead it in
* add date-paste and knead it in
* add nuts and oatmeal and knead them in
* add cacao powder and knead it in
* form balls with your hands and roll them in sesame or coconut flakes

{% comment %}
evtl.:

* Nüsse im voraus einweichen
* + Ingwer
{% endcomment %}

